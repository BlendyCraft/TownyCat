package com.blendycat.plugins.towns;

import com.blendycat.plugins.towns.commands.ResidentCommand;
import com.blendycat.plugins.towns.commands.TownCommand;
import com.blendycat.plugins.towns.sql.QueryManager;
import com.blendycat.plugins.towns.town.Plot;
import com.blendycat.plugins.towns.town.Town;
import com.blendycat.plugins.towns.town.TownListener;
import com.blendycat.plugins.towns.user.PlayerListener;
import com.blendycat.plugins.towns.user.Resident;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;
import java.util.HashMap;
import java.util.UUID;

public class Main extends JavaPlugin{

    public static Economy economy = null;
    private static Main instance;
    private static FileConfiguration config;

    private static HashMap<String, Resident> residents;
    private static HashMap<String, Town> towns;
    private static HashMap<UUID, Town> townIds;
    private static HashMap<Chunk, Plot> plots;

    @Override
    public void onEnable(){
        saveDefaultConfig();
        saveConfig();
        if(setupEconomy()){
            getLogger().info("Economy set up successfully!");
        }else{
            getLogger().warning(ChatColor.DARK_RED + "Error on setting up economy!");
        }
        instance = this;
        config = getConfig();

        residents = new HashMap<>();
        towns = new HashMap<>();
        townIds = new HashMap<>();
        plots = new HashMap<>();

        QueryManager.createTables();

        registerCommands();
        registerEvents();
    }


    private void registerCommands(){
        getCommand("town").setExecutor(new TownCommand());
        getCommand("resident").setExecutor(new ResidentCommand());
    }

    private void registerEvents(){
        PluginManager manager = Bukkit.getPluginManager();
        manager.registerEvents(new PlayerListener(), this);
        manager.registerEvents(new TownListener(), this);
    }

    @Override
    public void onDisable(){

    }

    private boolean setupEconomy()
    {
        RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
        if (economyProvider != null) {
            economy = economyProvider.getProvider();
        }

        return (economy != null);
    }
    public static String getDatabaseHost(){
        String host = null;
        if(config.contains("DB_Host")) {
            host = config.getString("DB_Host");
        }else{
            Bukkit.getPluginManager().disablePlugin(instance);
        }
        return host;
    }

    public static String getDatabaseName(){
        String name = null;
        if(config.contains("DB_Name")) {
            name = config.getString("DB_Name");
        }else{
            Bukkit.getPluginManager().disablePlugin(instance);
        }
        return name;
    }

    public static String getDatabaseUser(){
        String user = null;
        if(config.contains("DB_User")) {
            user = config.getString("DB_User");
        }else{
            Bukkit.getPluginManager().disablePlugin(instance);
        }
        return user;
    }

    public static String getDatabasePassword(){
        String password = null;
        if(config.contains("DB_Password")) {
            password = config.getString("DB_Password");
        }else{
            Bukkit.getPluginManager().disablePlugin(instance);
        }
        return password;
    }

    @NotNull
    public static Double getNewTownPrice(){
        return config.getDouble("town.new-price");
    }

    @NotNull
    public static Double getNewTownBank(){
        return config.getDouble("town.new-bank");
    }

    public static Resident getResident(String username){
        return residents.get(username);
    }

    public static void addResident(Player player, Resident resident){
        residents.put(player.getName(), resident);
    }

    public static void removeResident(Player player){
        residents.remove(player.getName());
    }

    public static void addTown(String name, Town town){
        towns.put(name, town);
        townIds.put(town.getUUID(), town);
    }

    public static void removeTown(String name){
        Town town = towns.remove(name);
        townIds.remove(town.getUUID());
    }

    public static Town getTown(String name){
        return towns.get(name);
    }

    public static void addPlot(Chunk chunk, Plot plot){
        plots.put(chunk, plot);
    }

    public static void removePlot(Chunk chunk){
        plots.remove(chunk);
    }

    public static Plot getPlot(Chunk chunk){
        return plots.get(chunk);
    }

    public static Town getTown(UUID id){
        return townIds.get(id);
    }
}
