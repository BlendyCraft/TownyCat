package com.blendycat.plugins.towns.sql;

import com.blendycat.plugins.towns.Main;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by EvanMerz on 10/19/17.
 */
class DatabaseManager {

    /**
     * remember to close after done with database
     * @return database connection
     */
    static Connection getConnection() {
        String DB_CONN_STRING = "jdbc:mysql://" + Main.getDatabaseHost() + ":3306/"+
                Main.getDatabaseName();
        String USER_NAME = Main.getDatabaseUser();
        String PASSWORD = Main.getDatabasePassword();
        Connection result = null;

        try {
            result = DriverManager.getConnection(DB_CONN_STRING, USER_NAME, PASSWORD);
        }
        catch (SQLException ex){
            ex.printStackTrace();
        }
        return result;
    }
}
