package com.blendycat.plugins.towns.sql;

import com.blendycat.plugins.towns.Main;
import com.blendycat.plugins.towns.town.Plot;
import com.blendycat.plugins.towns.town.Town;
import com.blendycat.plugins.towns.user.Resident;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import java.sql.*;
import java.util.ArrayList;
import java.util.UUID;

import static com.blendycat.plugins.towns.sql.DatabaseManager.getConnection;

public class QueryManager{

    public static void createTables(){
        Connection conn = getConnection();
        try{
            CallableStatement stmt = conn.prepareCall(
                    "CREATE TABLE IF NOT EXISTS `towns` (" +
                            "`id` INT NOT NULL AUTO_INCREMENT," +
                            "`uuid` VARCHAR(36) NOT NULL," +
                            "`name` VARCHAR(25) NOT NULL," +
                            "`owner` VARCHAR(36) NOT NULL," +
                            "`established` DATE NOT NULL," +
                            "`homeblock` VARCHAR(36) NOT NULL," +
                            "PRIMARY KEY(`id`)," +
                            "UNIQUE(`uuid`)" +
                         ");");
            stmt.execute();
            stmt = conn.prepareCall(
                    "CREATE TABLE IF NOT EXISTS `residents` (" +
                            "`id` INT NOT NULL AUTO_INCREMENT," +
                            "`uuid` VARCHAR(36) NOT NULL," +
                            "`town` VARCHAR(36)," +
                            "`rank` VARCHAR(16)," +
                            "`joined` DATE," +
                            "PRIMARY KEY(`id`)," +
                            "UNIQUE(`uuid`)" +
                    ");");
            stmt.execute();
            stmt = conn.prepareCall(
                    "CREATE TABLE IF NOT EXISTS `plots`(" +
                            "`id` INT NOT NULL AUTO_INCREMENT," +
                            "`uuid` VARCHAR(36) NOT NULL," +
                            "`town` VARCHAR(36) NOT NULL," +
                            "`owner` VARCHAR(36)," +
                            "`name` VARCHAR(25)," +
                            "`price` DOUBLE NOT NULL," +
                            "`permissions` VARCHAR(255)," +
                            "`x` INT NOT NULL," +
                            "`z` INT NOT NULL," +
                            "`world` VARCHAR(255) NOT NULL," +
                            "PRIMARY KEY(`id`)," +
                            "UNIQUE(`uuid`)" +
                            ");");
            stmt.execute();
            conn.close();
        }catch(SQLException ex){
            ex.printStackTrace();
        }
    }


    /**
     * Add a new town to the Database
     * @param town The town object to be inserted into the DB
     */
    public static void createTown(Town town){
        Connection conn = getConnection();
        try{
            PreparedStatement stmt = conn.prepareStatement("INSERT INTO `towns`(`uuid`, `name`, `owner`, `established`, `homeblock`) " +
                    "VALUES(?, ?, ?, ?, ?);");
            stmt.setString(1, town.getUUID().toString());
            stmt.setString(2, town.getName());
            stmt.setString(3, town.getOwner().getUniqueId().toString());
            stmt.setDate(4, town.getEstablished());
            stmt.setString(5, town.getHomeBlock().getUUID().toString());
            stmt.execute();
            conn.close();
        }catch(SQLException ex){
            ex.printStackTrace();
        }
    }

    /**
     *
     * @param plot to the DB
     */
    public static void createPlot(Plot plot){
        Connection conn = getConnection();
        try{
            PreparedStatement stmt = conn.prepareStatement("INSERT INTO `plots`(`uuid`,`town`, `price`, `permissions`, `x`, `z`, `world`) " +
                    "VALUES(?, ?, ?, ?, ?, ?, ?);");
            stmt.setString(1, plot.getUUID().toString());
            stmt.setString(2, plot.getTown().getUUID().toString());
            stmt.setDouble(3, -1);
            stmt.setString(4, plot.getPermissions());
            stmt.setInt(5, plot.getChunk().getX());
            stmt.setInt(6, plot.getChunk().getZ());
            stmt.setString(7, plot.getChunk().getWorld().getName());
            stmt.execute();
            conn.close();
        }catch(SQLException ex){
            ex.printStackTrace();
        }
    }

    /**
     * Get a resident object from the database
     * @param player the player that the Resident object is linked to
     * @return the Resident object
     */
    public static Resident getResident(OfflinePlayer player){
        Resident resident = null;
        Connection conn = getConnection();
        try{
            PreparedStatement stmt = conn.prepareStatement("SELECT `town`, `rank`, `joined` FROM `residents` WHERE `uuid`=?;");
            stmt.setString(1, player.getUniqueId().toString());
            stmt.execute();
            ResultSet rs = stmt.getResultSet();
            if(rs.next()){
                Town town = rs.getString(1) != null ? Main.getTown(UUID.fromString(rs.getString(1))) : null;
                String rank = rs.getString(1) != null ? rs.getString(2) : null;
                Date joined = rs.getDate(3);
                resident = new Resident(player, town, false, joined, rank);
            }
            conn.close();
        }catch(SQLException ex){
            ex.printStackTrace();
        }
        return resident;
    }

    /**
     * Adds new residents to the database
     * @param res the resident to be added
     */
    public static void createResident(Resident res){
        Connection conn = getConnection();
        try{
            PreparedStatement stmt = conn.prepareStatement("INSERT INTO `residents`(`uuid`, `joined`) VALUES(?, ?);");
            stmt.setString(1, res.getPlayer().getUniqueId().toString());
            stmt.setDate(2, res.getJoined());
            stmt.execute();
            conn.close();
        }catch(SQLException ex){
            ex.printStackTrace();
        }
    }

    public static void updateResident(Resident res){
        Connection conn = getConnection();
        try{
            PreparedStatement stmt = conn.prepareStatement("UPDATE `residents` SET `town`=?, `rank`=? WHERE `uuid`=?;");
            stmt.setString(1, res.getTown().getUUID().toString());
            stmt.setString(2, res.getRank());
            stmt.setString(3, res.getPlayer().getUniqueId().toString());
            stmt.execute();
            conn.close();
        }catch(SQLException ex){
            ex.printStackTrace();
        }
    }

    public static Plot getPlot(Chunk chunk){
        int x = chunk.getX();
        int z = chunk.getZ();
        Plot plot = null;
        Connection conn = getConnection();
        try{
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM `plots` WHERE `x`=? AND `z`=?;");
            stmt.setInt(1, x);
            stmt.setInt(2, z);
            stmt.execute();
            ResultSet rs = stmt.getResultSet();
            if(rs.next()){
                UUID uuid = UUID.fromString(rs.getString(2));
                UUID townId = rs.getString(3) == null ? null : UUID.fromString(rs.getString(3));
                UUID ownerId = rs.getString(4) == null ? null : UUID.fromString(rs.getString(4));
                Town town = townId == null ? null : Main.getTown(townId);
                OfflinePlayer player = ownerId == null ? null : Bukkit.getOfflinePlayer(ownerId);
                String name = rs.getString(5);
                double price = rs.getDouble(6);
                String permissions = rs.getString(7);
                plot = new Plot(uuid, chunk, town, player, price, permissions, name);
            }
        }catch(SQLException ex){
            ex.printStackTrace();
        }
        return plot;
    }

    public static ArrayList<Chunk> getClaimedChunks(){
        ArrayList<Chunk> chunks = new ArrayList<>();
        Connection conn = getConnection();
        try{
            CallableStatement stmt = conn.prepareCall("SELECT `x`, `z`, `world` FROM `plots`;");
            stmt.execute();
            ResultSet rs = stmt.getResultSet();
            while(rs.next()){
                int x = rs.getInt(1);
                int z = rs.getInt(2);
                String w = rs.getString(3);
                World world = Bukkit.getWorld(w);
                chunks.add(world.getChunkAt(x,z));
            }
            conn.close();
        }catch(SQLException ex){
            ex.printStackTrace();
        }
        return chunks;
    }
}
