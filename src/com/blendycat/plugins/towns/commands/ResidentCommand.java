package com.blendycat.plugins.towns.commands;

import com.blendycat.plugins.towns.Main;
import com.blendycat.plugins.towns.sql.QueryManager;
import com.blendycat.plugins.towns.user.Resident;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.text.SimpleDateFormat;

public class ResidentCommand implements CommandExecutor {


    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(sender instanceof Player){
            Player player = (Player) sender;
            OfflinePlayer requestedPlayer = null;
            if(args.length == 0){
                requestedPlayer = player;
            }else{
                String username = args[0];
                for(OfflinePlayer  p : Bukkit.getOfflinePlayers()){
                    if(p.getName().equalsIgnoreCase(username)){
                        requestedPlayer = p;
                        break;
                    }
                }
            }
            if(requestedPlayer != null) {
                Resident resident;
                if(requestedPlayer.isOnline()) {
                    resident = Main.getResident(requestedPlayer.getName());
                }else{
                    resident = QueryManager.getResident(requestedPlayer);
                }
                player.sendMessage(ChatColor.DARK_GREEN + "Username: " + ChatColor.WHITE + requestedPlayer.getName());
                player.sendMessage(ChatColor.DARK_GREEN + "Joined: " + ChatColor.WHITE + new SimpleDateFormat("MM/dd/yy").format(resident.getJoined()));
                player.sendMessage(ChatColor.DARK_GREEN + "Balance: " + ChatColor.WHITE + "$" + Main.economy.getBalance(requestedPlayer));
                player.sendMessage(ChatColor.DARK_GREEN + "Town: " + ChatColor.WHITE + (resident.getTown() == null ? "None" : resident.getTown().getName()));
            }else{
                player.sendMessage(ChatColor.DARK_RED + "That player doesn't exist!");
            }
        }else if(args.length == 0) {
            sender.sendMessage(ChatColor.RED + "You must specify a player!");
        }
        return true;
    }
}
