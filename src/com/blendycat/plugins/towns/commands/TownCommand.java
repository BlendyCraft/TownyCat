package com.blendycat.plugins.towns.commands;

import com.blendycat.plugins.towns.Main;
import com.blendycat.plugins.towns.sql.QueryManager;
import com.blendycat.plugins.towns.town.Town;
import com.blendycat.plugins.towns.user.Resident;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TownCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String label, String[] args) {
        if(commandSender instanceof Player){
            Player player = (Player) commandSender;
            // If the sub command is "create"
            if(args.length >= 1 && args[0].equalsIgnoreCase("create")){
                // Are there only 3 arguments?
                if(args.length == 2){
                    // Is the name 25 characters or less?
                    if(args[1].length() <= 25) {
                        // Check the name for invalid characters
                        boolean nameAccepted = true;
                        String name = args[1];
                        for(char c : name.toUpperCase().toCharArray()){
                            if(!Town.ACCEPTED_CHARACTERS.contains(c + "")){
                                nameAccepted = false;
                                break;
                            }
                        }
                        // If the name doesn't have any invalid characters
                        if(nameAccepted) {
                            Resident resident = Main.getResident(player.getName());
                            // Is the player in a town already?
                            if (!resident.hasTown()) {
                                // Does the player have enough money to create a town?
                                if (Main.economy.getBalance(player) >= Main.getNewTownPrice()) {
                                    Town town = new Town(player, player.getLocation().getChunk(), name);
                                    resident.setTown(town);
                                    resident.setRank("owner");
                                    Main.addTown(name, town);
                                    Main.economy.withdrawPlayer(player, Main.getNewTownPrice());
                                    QueryManager.createTown(town);
                                    QueryManager.updateResident(resident);
                                    player.sendMessage(ChatColor.GREEN + "Town created!");
                                } else {
                                    player.sendMessage(ChatColor.RED + "You don't have enough money to create a town!");
                                }
                            } else {
                                player.sendMessage(ChatColor.DARK_RED + "You cannot create a town if you are already in one! Leave your town before creating a new one.");
                            }
                        }else{
                            player.sendMessage("Name not accepted!");
                        }
                    }else{
                        player.sendMessage(ChatColor.DARK_RED + "Town name must be 25 characters or less!");
                    }
                }else{
                    player.sendMessage(ChatColor.DARK_RED + "Invalid Arguments! /town create <name>");
                }
            }
        }else{
            commandSender.sendMessage(ChatColor.DARK_RED + "This command is only for players!");
        }
        return true;
    }
}
