package com.blendycat.plugins.towns.user;

import com.blendycat.plugins.towns.town.Town;
import org.bukkit.OfflinePlayer;

import java.sql.Date;
import java.util.Calendar;


public class Resident {

    private OfflinePlayer player;
    private Town town;
    private Date joined;
    private String rank;

    public Resident(OfflinePlayer player, Town town, boolean isNew, Date joined, String rank){
        this.town = town;
        this.player = player;
        this.rank = rank;
        this.joined = joined;
        if(isNew) {
            this.joined = new Date(Calendar.getInstance().getTime().getTime());
        }
    }

    public Town getTown(){
        return town;
    }

    public String getRank(){
        return rank;
    }

    public void setTown(Town town){
        this.town = town;
    }

    public void setRank(String rank){
        this.rank = rank;
    }

    public OfflinePlayer getPlayer(){
        return player;
    }

    public Date getJoined(){
        return joined;
    }

    public boolean hasTown(){
        return town != null;
    }
}
