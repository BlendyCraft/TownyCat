package com.blendycat.plugins.towns.user;

import com.blendycat.plugins.towns.Main;
import com.blendycat.plugins.towns.sql.QueryManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerListener implements Listener {

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e){
        // We need to check if the user is new or not to determine how to handle this event
        Player player = e.getPlayer();
        if(player.hasPlayedBefore()){
            Resident res = QueryManager.getResident(player);
            Main.addResident(player, res);
        }else{
            // Create a new resident and plop that mo'fo in the database
            Resident res = new Resident(player, null, true, null, null);
            Main.addResident(player, res);
            QueryManager.createResident(res);
        }
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e){
        // Remove the player from the HashMap to store data
        Main.removeResident(e.getPlayer());
    }
}
