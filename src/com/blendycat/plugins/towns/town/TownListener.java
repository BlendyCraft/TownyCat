package com.blendycat.plugins.towns.town;

import com.blendycat.plugins.towns.Main;
import com.blendycat.plugins.towns.sql.QueryManager;
import org.bukkit.Chunk;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.event.world.ChunkUnloadEvent;

import java.util.ArrayList;

public class TownListener implements Listener {

    private ArrayList<Chunk> chunks;

    public TownListener(){
        chunks = QueryManager.getClaimedChunks();
    }


    @EventHandler
    public void onChunkLoad(ChunkLoadEvent e){
        Chunk chunk = e.getChunk();
        if(chunks.contains(chunk)) {
            Plot plot = QueryManager.getPlot(chunk);
            if (plot != null) {
                Main.addPlot(chunk, plot);
            }
        }
    }

    @EventHandler
    public void onChunkUnload(ChunkUnloadEvent e){
        if(chunks.contains(e.getChunk())) {
            Main.removePlot(e.getChunk());
        }
    }
}
