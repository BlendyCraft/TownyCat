package com.blendycat.plugins.towns.town;

import com.blendycat.plugins.towns.Main;
import org.bukkit.Chunk;
import org.bukkit.OfflinePlayer;

import java.util.UUID;

public class Plot {

    private Chunk chunk;
    private UUID uuid;
    private Town town;
    private OfflinePlayer owner;
    private double price;
    private String permissions;
    private String name;

    /**
     * Claim a chunk to a town
     * @param chunk the chunk being claimed
     * @param town which the plot is being claimed to
     */
    Plot(Chunk chunk, Town town){
        this.chunk = chunk;
        this.town = town;
        uuid = UUID.randomUUID();
        permissions = "FFFFF";
        Main.addPlot(chunk, this);
    }

    /**
     * Plot already exists; is being initialized
     * @param chunk the chunk the plot is
     * @param town the town the plot is in
     * @param owner the owner of the plot; nullable
     * @param price  the price of the plot; -1 is not for sale
     * @param permissions the string of permissions
     */
    public Plot(UUID uuid, Chunk chunk, Town town, OfflinePlayer owner, double price, String permissions, String name){
        this.chunk = chunk;
        this.town = town;
        this.owner = owner;
        this.price = price;
        this.permissions = permissions;
        this.name = name;
        this.uuid = uuid;
    }

    public Chunk getChunk(){
        return chunk;
    }

    public Town getTown(){
        return town;
    }

    public UUID getUUID(){
        return uuid;
    }

    public OfflinePlayer getOwner(){
        return owner;
    }

    public boolean isForSale(){
        return price >= 0;
    }

    public double getPrice(){
        return price;
    }

    public boolean canFriendDestroy(){
        return permissions.charAt(0) == 'F';
    }

    public boolean canFriendPlace(){
        return permissions.charAt(1) == 'F';
    }

    public boolean canFriendOpen(){
        return permissions.charAt(2) == 'F';
    }

    public boolean canFriendSwitch(){
        return permissions.charAt(3) == 'F';
    }

    public boolean canFriendUse(){
        return permissions.charAt(4) == 'F';
    }

    public String getName(){
        return name;
    }

    public String getPermissions(){
        return permissions;
    }

}
