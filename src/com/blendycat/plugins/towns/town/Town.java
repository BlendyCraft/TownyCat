package com.blendycat.plugins.towns.town;

import com.blendycat.plugins.towns.Main;
import com.blendycat.plugins.towns.sql.QueryManager;
import org.bukkit.Chunk;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.sql.Date;
import java.util.Calendar;
import java.util.UUID;

public class Town {
    private OfflinePlayer owner;
    private Plot homeBlock;
    private String name;
    private UUID uuid;
    private Date est;
    public static final String ACCEPTED_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ_123456789";

    public Town(Player owner, Chunk homeBlock, String name){
        this.owner = owner;
        this.name = name;
        this.homeBlock = new Plot(homeBlock, this);
        Main.economy.createBank("town-" + this.name, owner);
        Main.economy.bankDeposit("town-"+this.name, Main.getNewTownBank());
        est = new Date(Calendar.getInstance().getTime().getTime());
        uuid = UUID.randomUUID();
        QueryManager.createPlot(this.homeBlock);
        Main.addPlot(homeBlock, this.homeBlock);
    }

    public Town(Player owner, Plot homeBlock, String name, UUID uuid, Date est){
        this.owner = owner;
        this.name = name;
        this.homeBlock = homeBlock;
        this.est = est;
        this.uuid = uuid;
    }

    public Plot getHomeBlock(){
        return homeBlock;
    }

    public OfflinePlayer getOwner() {
        return owner;
    }

    public String getName() {
        return name;
    }

    public Date getEstablished(){
        return est;
    }

    public UUID getUUID(){
        return uuid;
    }

}
